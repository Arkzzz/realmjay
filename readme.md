
![RealmJay](https://i.imgur.com/CBPoEVX.png)
##A java based Realm of the Mad God networking library.
[discord-invite]: https://discord.gg/kaHTbqa

## TODO
**Warning, projectile handling is not fully functional in this version use at your own risk!**
 - Documentation
 - Finish move records/projectile handling
## Building
Run `mvn package` or run maven package process in your IDE
## Thanks to:
 - Killer Be Killed - created nrelay and n2 which inspired me to make this.
 - Bert - Help with crypto implementations and some other stuff. 
