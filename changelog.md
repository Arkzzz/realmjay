# Changelog
This changelog uses systematic version string.
## 1.4.0
 - Refactored an extremely numerable amount of classes, the notes for future versions will be more comprehensive.
## 1.3.2-SNAPSHOT
 - Major refactoring of `CombatClient`/`Client` and entity/object/statdata that I am not going to go into full detail on.
## 1.3.1-SNAPSHOT
 - Lots of minor typo fixes, standardizations, and refactoring.
 - Added `ProxyIO` for use in an MITM proxy.
 - `PacketIO` will now only read packets when necacary;
## 1.3.0-SNAPSHOT
 - Add proxy support to `PacketIO` and `Http`
 - Refactored:
    - `Mapper` to `PacketMapper`
    - `HookStore` to `ListenerStore`
    - `PacketHook` to `PacketListener`
    - `GameData` to `GameDataMapper`
    - Removed `NotNull`
 - `PacketIO` is now implements the runable interface instead of extending thread.
 - Added option to all mappers to map from external file.
 - Added `CondtionEffect` enum, used to decode condition effects.
 - Added `MoveRecords`
 - Added `CombatClient` a less lightweight client that has features needed to fight enemies.
 - Reworked parsers to use xmls
 - Added dependency `org.jdom`
 - Removed dependency `org.json`
## 1.2.0
 - Refactored many models to live in `ark.realmjay.game`
 - Added static game data parsing from JSON - see `ark.realmjay.game.data.GameInfoMapper` - updated JSON data can be retried from https://static.drips.pw/production/current
 - Started creating classes that have to do with projectile handling.
## 1.1.1
 - Tweaked receiving end of `PacketIO` to not print a null pointer exception and disconnect if there are not enough bytes in the input buffer to fully read to the packet's buffer, and will now instead wait until there is enough bytes. 
## 1.1.0
 - Redid `PacketIO` to use `HookStore` instead of storing its own hooks.
 - Client can now reconnect without throwing thread exceptions.
 - Removed `PacketIO.hook` method.
 - `PacketIO` constructor takes a new argument `HookStore`.
 - `Client` now has its own `HookStore` that is passed to `PacketIO` and can be used for hooking methods. Ex: `this.hs.hook(PacketType.HELLO, p -> ...);`
## 1.0.0
 - Fixed PacketIO memory issues
 - Old PacketIO class still accessable via `ark.realmjay.net.PacketIOLegacy`
 - Added connect method to PacketIO
 - Added disconnect method to PacketIO
## Indev-Beta 0.3
 - Added sleep time in while loop in `PacketIO` to prevent intense cpu/memory hogging.
 - `StatusParser` for player objects
 - Added appspot related features:
 - String to  `XML` doc parser
 - Appspot `Endpoints` collection
 - `EndpointFactory` to add querys to endpoints
 - `ServerMapper` loads list of servers from char/list
## Indev-Beta 0.2
 - Client is now able to fully connect to game server (fixed issues in packetIO and some packets)
 - Switched over from ArrayLists to standard Arrays
## Indev-Beta 0.1
 - No more PacketBuffer, instead using java.io DataInput and DataOutput for DataPacket
 - Finished packet reciving PacketIO can now recieve packets
 - PacketIO.hook(PacketType, Method<Packet>) attatches a hook to a type of packet
 - PacketIO.send(Packet) sends a packet
