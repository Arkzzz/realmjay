package ark.realmjay;

import ark.realmjay.util.logging.LogLevel;

public class Environment {
    public static final LogLevel MIN_LOG_LEVEL = LogLevel.PROD;
    public static final String RC4_INCOMING_KEY = "c79332b197f92ba85ed281a023";
    public static final String RC4_OUTGOING_KEY = "6a39570cc9de4ec71d64821894";
}
