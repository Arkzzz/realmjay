package ark.realmjay.impl;

import ark.realmjay.Environment;
import ark.realmjay.util.logging.Logger;
import ark.realmjay.game.models.Server;
import ark.realmjay.net.ProxyIO;
import ark.realmjay.net.listeners.ListenerStore;
import ark.realmjay.net.listeners.ListenerType;
import ark.realmjay.net.packets.PacketType;
import ark.realmjay.net.packets.incoming.MapInfoPacket;
import ark.realmjay.net.packets.incoming.TextPacket;

import java.io.IOException;
import java.net.Socket;

@SuppressWarnings("WeakerAccess")
public class Proxy implements Runnable {
    @SuppressWarnings("WeakerAccess")
    protected ProxyIO client;
    @SuppressWarnings("WeakerAccess")
    protected ProxyIO server;

    @SuppressWarnings("WeakerAccess")
    protected ListenerStore ls;

    @SuppressWarnings("WeakerAccess")
    protected boolean connected;

    public Proxy(Socket clientSocket, Server server) throws IOException {
        this.ls = new ListenerStore();
        this.server = new ProxyIO(new Socket(server.getHost(), server.getPort()), this.ls, Environment.RC4_OUTGOING_KEY, Environment.RC4_INCOMING_KEY);
        this.client = new ProxyIO(clientSocket, this.ls, Environment.RC4_INCOMING_KEY, Environment.RC4_OUTGOING_KEY);
        this.server.setPartner(this.client);
        this.client.setPartner(this.server);

        this.ls.hook(ListenerType.DISCONNECT, () -> {
            this.client.disconnect();
            this.server.disconnect();
            this.connected = false;
        });

        this.ls.hook(PacketType.MAPINFO, p -> {
            MapInfoPacket mapInfo = (MapInfoPacket)p;
            Logger.log("Proxy", String.format("Connected to %s %s", server.getName(), mapInfo.name));
            TextPacket tp = new TextPacket();
            tp.name = "RealmJay";
            tp.numStars = -1;
            tp.cleanText = "Welcome to RealmJay Proxy!";
            tp.text = "Welcome to RealmJay Proxy!";
            tp.objectId = -1;
            this.client.send(tp);
        });
    }

    public ProxyIO getClient() {
        return client;
    }

    public ProxyIO getServer() {
        return server;
    }

    public ListenerStore getListenerStore() {
        return ls;
    }

    @Override
    public void run() {
        this.client.start();
        this.server.start();
        this.connected = true;
    }
}
