package ark.realmjay.net.listeners;

import ark.realmjay.net.data.Packet;
import ark.realmjay.net.packets.PacketType;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ListenerStore {
    private Map<PacketType, ArrayList<PacketListener<? extends Packet>>> packetMap;
    private Map<ListenerType, ArrayList<IOListener>> ioMap;

    public ListenerStore() {
        this.packetMap = new HashMap<>();
        this.ioMap = new HashMap<>();
    }

    public void hook(PacketType type, PacketListener<? extends Packet> method) {
        if(!this.packetMap.containsKey(type)) {
            this.packetMap.put(type, new ArrayList<>());
        }
        this.packetMap.get(type).add(method);
    }

    public void hook(ListenerType type, IOListener method) {
        if(!this.ioMap.containsKey(type)) {
            this.ioMap.put(type, new ArrayList<>());
        }
        this.ioMap.get(type).add(method);
    }

    public void invoke(ListenerType type) throws Exception {
        if (this.ioMap.containsKey(type)) {
            for (IOListener method : this.ioMap.get(type)) {
                method.call();
            }
        }
    }

    public void invoke(Packet p) throws IOException {
        if (this.packetMap.containsKey(p.getType())) {
            for (PacketListener method : this.packetMap.get(p.getType())) {
                //noinspection unchecked
                method.call(p);
            }
        }
    }

    public boolean has(PacketType pt) {
        return this.packetMap.containsKey(pt);
    }
}
