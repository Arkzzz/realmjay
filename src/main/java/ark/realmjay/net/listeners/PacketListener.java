package ark.realmjay.net.listeners;

import ark.realmjay.net.data.Packet;

import java.io.IOException;

public interface PacketListener<T extends Packet> {
    void call(T p) throws IOException;
}
