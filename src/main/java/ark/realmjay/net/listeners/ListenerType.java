package ark.realmjay.net.listeners;

public enum ListenerType {
    CONNECT,
    DISCONNECT
}
