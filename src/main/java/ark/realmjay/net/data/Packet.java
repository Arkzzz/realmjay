package ark.realmjay.net.data;

import ark.realmjay.net.packets.PacketType;

public interface Packet extends DataPacket {
    PacketType getType();
}
