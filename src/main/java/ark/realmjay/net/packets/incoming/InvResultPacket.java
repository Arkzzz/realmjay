package ark.realmjay.net.packets.incoming;

import ark.realmjay.net.data.Packet;
import ark.realmjay.net.packets.PacketType;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

@SuppressWarnings("ALL")
public class InvResultPacket implements Packet {
    public int result;

    public InvResultPacket() { }

    public PacketType getType() {
        return PacketType.INVRESULT;
    }

    public void read(DataInput in) throws IOException {
        this.result = in.readInt();
    }

    public void write(DataOutput out) throws IOException  {
        out.writeInt(this.result);
    }
}
