package ark.realmjay.net.packets.incoming;

import ark.realmjay.net.data.Packet;
import ark.realmjay.net.packets.PacketType;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

@SuppressWarnings("ALL")
public class FailurePacket implements Packet {
    public int errorId;

    public String errorDescription;

    public FailurePacket() { }

    public PacketType getType() {
        return PacketType.FAILURE;
    }

    public void read(DataInput in) throws IOException {
        this.errorId = in.readInt();
        this.errorDescription = in.readUTF();
    }

    public void write(DataOutput out) throws IOException  {
        out.writeInt(this.errorId);
        out.writeUTF(this.errorDescription);
    }
}
