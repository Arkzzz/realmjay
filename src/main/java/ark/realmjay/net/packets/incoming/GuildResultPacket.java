package ark.realmjay.net.packets.incoming;

import ark.realmjay.net.data.Packet;
import ark.realmjay.net.packets.PacketType;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

@SuppressWarnings("ALL")
public class GuildResultPacket implements Packet {
    public boolean success;

    public String lineBuilderJSON;

    public GuildResultPacket() { }

    public PacketType getType() {
        return PacketType.GUILDRESULT;
    }

    public void read(DataInput in) throws IOException {
        this.success = in.readBoolean();
        this.lineBuilderJSON = in.readUTF();
    }

    public void write(DataOutput out) throws IOException  {
        out.writeBoolean(this.success);
        out.writeUTF(this.lineBuilderJSON);
    }
}
