package ark.realmjay.net.packets.incoming;

import ark.realmjay.net.data.Packet;
import ark.realmjay.net.packets.PacketType;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

@SuppressWarnings("WeakerAccess")
public class ChatTokenPacket implements Packet {
    public String token;
    public String host;
    public int port;

    public PacketType getType() {
        return PacketType.CHATTOKENMSG;
    }

    public ChatTokenPacket() { }

    public void read(DataInput in) throws IOException {
        this.token = in.readUTF();
        this.host = in.readUTF();
        this.port = in.readInt();
    }

    public void write(DataOutput out) throws IOException {
        out.writeUTF(this.token);
        out.writeUTF(this.host);
        out.writeInt(this.port);
    }
}
