package ark.realmjay.net.packets.incoming;

import ark.realmjay.net.data.Packet;
import ark.realmjay.net.packets.PacketType;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

@SuppressWarnings("ALL")
public class GlobalNotificationPacket implements Packet {
    public int notificationType;

    public String text;

    public GlobalNotificationPacket() { }

    public PacketType getType() {
        return PacketType.GLOBALNOTIFICATION;
    }

    public void read(DataInput in) throws IOException {
        this.notificationType = in.readInt();
        this.text = in.readUTF();
    }

    public void write(DataOutput out) throws IOException  {
        out.writeInt(this.notificationType);
        out.writeUTF(this.text);
    }
}
