package ark.realmjay.net.packets.incoming;

import ark.realmjay.net.data.Packet;
import ark.realmjay.net.packets.PacketType;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

@SuppressWarnings("WeakerAccess")
public class BuyResultPacket implements Packet {
    public int result;

    public String resultString;

    public BuyResultPacket() { }

    public PacketType getType() {
        return PacketType.BUYRESULT;
    }

    public void read(DataInput in) throws IOException {
        this.result = in.readInt();
        this.resultString = in.readUTF();
    }

    public void write(DataOutput out) throws IOException {
        out.writeInt(this.result);
        out.writeUTF(this.resultString);
    }
}
