package ark.realmjay.net.packets.incoming;

import ark.realmjay.net.data.Packet;
import ark.realmjay.net.packets.PacketType;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

@SuppressWarnings("ALL")
public class PasswordPromptPacket implements Packet {
    public int cleanPasswordStatus;

    public PacketType getType() {
        return PacketType.PASSWORDPROMPT;
    }

    public PasswordPromptPacket() { }

    public void read(DataInput in) throws IOException {
        this.cleanPasswordStatus = in.readInt();
    }

    public void write(DataOutput out) throws IOException  {
        out.writeInt(this.cleanPasswordStatus);
    }
}
