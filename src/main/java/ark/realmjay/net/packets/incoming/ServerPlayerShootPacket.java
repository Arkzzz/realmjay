package ark.realmjay.net.packets.incoming;

import ark.realmjay.net.data.Packet;
import ark.realmjay.net.data.WorldPosData;
import ark.realmjay.net.packets.PacketType;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class ServerPlayerShootPacket implements Packet {
    public byte bulletId;

    public int ownerId;

    public int containerType;

    public WorldPosData startingPos;

    public float angle;

    public short dammage;

    public PacketType getType() {
        return PacketType.SERVERPLAYERSHOOT;
    }

    public ServerPlayerShootPacket() { }

    public void read(DataInput in) throws IOException {
        this.bulletId = in.readByte();
        this.ownerId = in.readInt();
        this.containerType = in.readInt();
        this.startingPos = new WorldPosData();
        this.startingPos.read(in);
        this.angle = in.readFloat();
        this.dammage = in.readShort();
    }

    public void write(DataOutput out) throws IOException  {
        out.writeByte(this.bulletId);
        out.writeInt(this.ownerId);
        out.writeInt(this.containerType);
        this.startingPos.write(out);
        out.writeFloat(this.angle);
        out.writeShort(this.dammage);
    }
}
