package ark.realmjay.net.packets.incoming;

import ark.realmjay.net.data.Packet;
import ark.realmjay.net.packets.PacketType;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class PingPacket implements Packet {
    public int serial;

    public PacketType getType() {
        return PacketType.PING;
    }

    public PingPacket() { }

    public void read(DataInput in) throws IOException {
        this.serial = in.readInt();
    }

    public void write(DataOutput out) throws IOException  {
        out.writeInt(this.serial);
    }
}
