package ark.realmjay.net.packets.incoming;

import ark.realmjay.net.data.Packet;
import ark.realmjay.net.packets.PacketType;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

@SuppressWarnings("ALL")
public class NotificationPacket implements Packet {
    public int objectId;

    public String message;

    public int color;

    public PacketType getType() {
        return PacketType.NOTIFICATION;
    }

    public NotificationPacket() { }

    public void read(DataInput in) throws IOException {
        this.objectId = in.readInt();
        this.message = in.readUTF();
        this.color = in.readInt();
    }

    public void write(DataOutput out) throws IOException  {
        out.writeInt(this.objectId);
        out.writeUTF(this.message);
        out.writeInt(this.color);
    }
}
