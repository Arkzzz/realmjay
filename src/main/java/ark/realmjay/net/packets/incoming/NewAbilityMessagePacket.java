package ark.realmjay.net.packets.incoming;

import ark.realmjay.net.data.Packet;
import ark.realmjay.net.packets.PacketType;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

@SuppressWarnings("ALL")
public class NewAbilityMessagePacket implements Packet {
    public int abilityType;

    public NewAbilityMessagePacket() { }

    public PacketType getType() {
        return PacketType.NEWABILITY;
    }

    public void read(DataInput in) throws IOException {
        this.abilityType = in.readInt();
    }

    public void write(DataOutput out) throws IOException  {
        out.writeInt(this.abilityType);
    }
}
