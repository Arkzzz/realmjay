package ark.realmjay.net.packets.incoming;

import ark.realmjay.net.data.Packet;
import ark.realmjay.net.packets.PacketType;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class TradeRequestedPacket implements Packet {
    public String name;

    public PacketType getType() {
        return PacketType.TRADEREQUESTED;
    }

    public TradeRequestedPacket() { }

    public void read(DataInput in) throws IOException {
        this.name = in.readUTF();
    }

    public void write(DataOutput out) throws IOException  {
        out.writeUTF(this.name);
    }
}
