package ark.realmjay.net.packets.incoming;

import ark.realmjay.net.data.Packet;
import ark.realmjay.net.packets.PacketType;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

@SuppressWarnings("WeakerAccess")
public class ClientStatPacket implements Packet {
    public String name;

    public int value;

    public ClientStatPacket() { }

    public PacketType getType() {
        return PacketType.CLIENTSTAT;
    }

    public void read(DataInput in) throws IOException {
        this.name = in.readUTF();
        this.value = in.readInt();
    }

    public void write(DataOutput out) throws IOException  {
        out.writeUTF(this.name);
        out.writeInt(this.value);
    }
}
