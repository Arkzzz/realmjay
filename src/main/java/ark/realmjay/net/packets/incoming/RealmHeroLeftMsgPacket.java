package ark.realmjay.net.packets.incoming;

import ark.realmjay.net.data.Packet;
import ark.realmjay.net.packets.PacketType;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class RealmHeroLeftMsgPacket implements Packet {
    public int herosLeft;

    public PacketType getType() {
        return PacketType.REALMHEROLEFTMSG;
    }

    public void read(DataInput in) throws IOException {
        this.herosLeft = in.readInt();
    }

    public void write(DataOutput out) throws IOException {
        out.write(this.herosLeft);
    }
}
