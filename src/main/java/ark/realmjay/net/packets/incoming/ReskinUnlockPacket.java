package ark.realmjay.net.packets.incoming;

import ark.realmjay.net.data.Packet;
import ark.realmjay.net.packets.PacketType;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class ReskinUnlockPacket implements Packet {
    public int skinId;

    public PacketType getType() {
        return PacketType.RESKINUNLOCK;
    }

    public ReskinUnlockPacket() { }

    public void read(DataInput in) throws IOException {
        this.skinId = in.readInt();
    }

    public void write(DataOutput out) throws IOException  {
        out.writeInt(this.skinId);
    }
}
