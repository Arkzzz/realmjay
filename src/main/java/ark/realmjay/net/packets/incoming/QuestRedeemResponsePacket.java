package ark.realmjay.net.packets.incoming;

import ark.realmjay.net.data.Packet;
import ark.realmjay.net.packets.PacketType;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class QuestRedeemResponsePacket implements Packet {
    public boolean ok;

    public String message;

    public PacketType getType() {
        return PacketType.QUESTREDEEMRESPONSE;
    }

    public void read(DataInput in) throws IOException {
        this.ok = in.readBoolean();
        this.message = in.readUTF();
    }

    public void write(DataOutput out) throws IOException  {
        out.writeBoolean(this.ok);
        out.writeUTF(this.message);
    }
}
