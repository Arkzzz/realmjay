package ark.realmjay.net.packets.incoming;

import ark.realmjay.net.data.Packet;
import ark.realmjay.net.packets.PacketType;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class QuestObjectIdPacket implements Packet {
    public int objectId;

    public PacketType getType() {
        return PacketType.QUESTOBJID;
    }

    public QuestObjectIdPacket() { }

    public void read(DataInput in) throws IOException {
        this.objectId = in.readInt();
    }

    public void write(DataOutput out) throws IOException  {
        out.writeInt(this.objectId);
    }
}
