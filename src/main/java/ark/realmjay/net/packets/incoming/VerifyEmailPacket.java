package ark.realmjay.net.packets.incoming;

import ark.realmjay.net.data.Packet;
import ark.realmjay.net.packets.PacketType;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class VerifyEmailPacket implements Packet {
    public PacketType getType() {
        return PacketType.VERIFYEMAIL;
    }

    public VerifyEmailPacket() { }

    public void read(DataInput in) throws IOException { }

    public void write(DataOutput out) throws IOException  { }
}
