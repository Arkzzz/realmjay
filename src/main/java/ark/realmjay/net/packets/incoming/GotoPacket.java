package ark.realmjay.net.packets.incoming;

import ark.realmjay.net.data.Packet;
import ark.realmjay.net.data.WorldPosData;
import ark.realmjay.net.packets.PacketType;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class GotoPacket implements Packet {
    public int objectId;

    public WorldPosData pos;

    public GotoPacket() { }

    public PacketType getType() {
        return PacketType.GOTO;
    }

    public void read(DataInput in) throws IOException {
        this.objectId = in.readInt();
        this.pos = new WorldPosData();
        this.pos.read(in);
    }

    public void write(DataOutput out) throws IOException  {
        out.writeInt(this.objectId);
        this.pos.write(out);
    }
}
