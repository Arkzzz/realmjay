package ark.realmjay.net.packets.incoming;

import ark.realmjay.net.data.Packet;
import ark.realmjay.net.packets.PacketType;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class TextPacket implements Packet {
    public String name;

    public int objectId;

    public int numStars;

    public byte bubbleTime;

    public String recipient;

    public String text;

    public String cleanText;

    public boolean isSupporter;

    public PacketType getType() {
        return PacketType.TEXT;
    }

    public TextPacket() {
        this.name = "";
        this.objectId = -1;
        this.numStars = -1;
        this.bubbleTime = 0;
        this.recipient = "";
        this.text = "";
        this.cleanText = "";
        this.isSupporter = false;
    }

    public void read(DataInput in) throws IOException {
        this.name = in.readUTF();
        this.objectId = in.readInt();
        this.numStars = in.readInt();
        this.bubbleTime = in.readByte();
        this.recipient = in.readUTF();
        this.text = in.readUTF();
        this.cleanText = in.readUTF();
        this.isSupporter = in.readBoolean();
    }

    public void write(DataOutput out) throws IOException  {
        out.writeUTF(this.name);
        out.writeInt(this.objectId);
        out.writeInt(this.numStars);
        out.writeByte(this.bubbleTime);
        out.writeUTF(this.recipient);
        out.writeUTF(this.text);
        out.writeUTF(this.cleanText);
        out.writeBoolean(this.isSupporter);
    }
}
