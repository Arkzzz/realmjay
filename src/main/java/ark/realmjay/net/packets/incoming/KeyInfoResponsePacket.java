package ark.realmjay.net.packets.incoming;

import ark.realmjay.net.data.Packet;
import ark.realmjay.net.packets.PacketType;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

@SuppressWarnings("ALL")
public class KeyInfoResponsePacket implements Packet {
    public String name;

    public String description;

    public String creator;

    public KeyInfoResponsePacket() { }

    public PacketType getType() {
        return PacketType.KEYINFORESPONSE;
    }

    public void read(DataInput in) throws IOException {
        this.name = in.readUTF();
        this.description = in.readUTF();
        this.creator = in.readUTF();
    }

    public void write(DataOutput out) throws IOException  {
        out.writeUTF(this.name);
        out.writeUTF(this.description);
        out.writeUTF(this.creator);
    }
}
