package ark.realmjay.net.packets.incoming;

import ark.realmjay.net.data.Packet;
import ark.realmjay.net.packets.PacketType;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class CreateSuccessPacket implements Packet {
    public int objectId;

    public int charId;

    public CreateSuccessPacket() { }

    public PacketType getType() {
        return PacketType.CREATESUCCESS;
    }

    public void read(DataInput in) throws IOException {
        this.objectId = in.readInt();
        this.charId = in.readInt();
    }

    public void write(DataOutput out) throws IOException  {
        out.writeInt(this.objectId);
        out.writeInt(this.charId);
    }
}
