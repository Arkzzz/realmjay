package ark.realmjay.net.packets.outgoing;

import ark.realmjay.net.data.Packet;
import ark.realmjay.net.packets.PacketType;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class TeleportPacket implements Packet {
    public int objectId;

    public TeleportPacket() { }

    public PacketType getType() {
        return PacketType.TELEPORT;
    }

    public void write(DataOutput out) throws IOException {
        out.writeInt(this.objectId);
    }

    public void read(DataInput in) throws IOException {
        this.objectId = in.readInt();
    }
}
