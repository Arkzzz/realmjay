package ark.realmjay.net.packets.outgoing;

import ark.realmjay.net.data.Packet;
import ark.realmjay.net.packets.PacketType;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class CreatePacket implements Packet {
    public short classType;
    public short skinType;

    public PacketType getType() {
        return PacketType.CREATE;
    }

    public CreatePacket() { }

    public void write(DataOutput out) throws IOException {
        out.writeShort(this.classType);
        out.writeShort(this.skinType);
    }

    public void read(DataInput in) throws IOException {
        this.classType = in.readShort();
        this.skinType = in.readShort();
    }
}
