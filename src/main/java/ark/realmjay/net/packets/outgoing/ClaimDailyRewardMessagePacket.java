package ark.realmjay.net.packets.outgoing;

import ark.realmjay.net.data.Packet;
import ark.realmjay.net.packets.PacketType;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

@SuppressWarnings("WeakerAccess")
public class ClaimDailyRewardMessagePacket implements Packet {
    public String claimKey;
    public String claimType;

    public PacketType getType() {
        return PacketType.CLAIMLOGINREWARDMSG;
    }

    public ClaimDailyRewardMessagePacket() { }

    public void write(DataOutput out) throws IOException {
        out.writeUTF(this.claimKey);
        out.writeUTF(this.claimType);
    }

    public void read(DataInput in) throws IOException {
        this.claimKey = in.readUTF();
        this.claimType = in.readUTF();
    }
}
