package ark.realmjay.net.packets.outgoing;

import ark.realmjay.net.data.Packet;
import ark.realmjay.net.packets.PacketType;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

@SuppressWarnings("WeakerAccess")
public class ChangeTradePacket implements Packet {
    public boolean[] offer;

    @Override
    public PacketType getType() {
        return PacketType.CHANGETRADE;
    }

    public ChangeTradePacket() {
        this.offer = new boolean[12];
    }

    @Override
    public void write(DataOutput out) throws IOException {
        out.writeShort(this.offer.length);
        for(boolean o : this.offer) {
            out.writeBoolean(o);
        }
    }

    @Override
    public void read(DataInput in) throws IOException {
        this.offer = new boolean[in.readShort()];
        for(short i = 0; i < this.offer.length; i++) {
            this.offer[i] = in.readBoolean();
        }
    }
}
