package ark.realmjay.net.packets.outgoing;

import ark.realmjay.net.data.Packet;
import ark.realmjay.net.packets.PacketType;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class EscapePacket implements Packet {
    public PacketType getType() {
        return PacketType.ESCAPE;
    }

    public EscapePacket() { }

    public void read(DataInput in) throws IOException { }

    public void write(DataOutput out) throws IOException { }
}
