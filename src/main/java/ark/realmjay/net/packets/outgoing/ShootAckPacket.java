package ark.realmjay.net.packets.outgoing;

import ark.realmjay.net.data.Packet;
import ark.realmjay.net.packets.PacketType;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class ShootAckPacket implements Packet {
    public int time;

    public ShootAckPacket() { }

    public PacketType getType() {
        return PacketType.SHOOTACK;
    }

    public void write(DataOutput out) throws IOException {
        out.writeInt(this.time);
    }

    public void read(DataInput in) throws IOException {
        this.time = in.readInt();
    }
}
