package ark.realmjay.net.packets.outgoing;

import ark.realmjay.net.data.Packet;
import ark.realmjay.net.packets.PacketType;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

@SuppressWarnings("ALL")
public class KeyInfoRequestPacket implements Packet {
    @SuppressWarnings("WeakerAccess")
    public int itemType;

    public PacketType getType() {
        return PacketType.KEYINFOREQUEST;
    }

    public KeyInfoRequestPacket() { }

    public void write(DataOutput out) throws IOException {
        out.writeInt(this.itemType);
    }

    public void read(DataInput in) throws IOException {
        this.itemType = in.readInt();
    }
}
