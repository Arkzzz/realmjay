package ark.realmjay.net.packets.outgoing;

import ark.realmjay.net.data.Packet;
import ark.realmjay.net.packets.PacketType;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class ReskinPacket implements Packet {
    public int skinId;

    public ReskinPacket() { }

    public PacketType getType() {
        return PacketType.RESKIN;
    }

    public void write(DataOutput out) throws IOException {
        out.writeInt(this.skinId);
    }

    public void read(DataInput in) throws IOException {
        this.skinId = in.readInt();
    }
}
