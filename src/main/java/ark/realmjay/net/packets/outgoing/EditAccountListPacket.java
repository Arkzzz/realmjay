package ark.realmjay.net.packets.outgoing;

import ark.realmjay.net.data.Packet;
import ark.realmjay.net.packets.PacketType;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

@SuppressWarnings("ALL")
public class EditAccountListPacket implements Packet {
    public int accountListId;
    public boolean add;
    public int objectId;

    public PacketType getType() {
        return PacketType.EDITACCOUNTLIST;
    }

    public EditAccountListPacket() { }

    public void write(DataOutput out) throws IOException {
        out.writeInt(this.accountListId);
        out.writeBoolean(this.add);
        out.writeInt(this.objectId);
    }

    public void read(DataInput in) throws IOException {
        this.accountListId = in.readInt();
        this.add = in.readBoolean();
        this.objectId = in.readInt();
    }
}
