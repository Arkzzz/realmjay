package ark.realmjay.net.packets.outgoing;

import ark.realmjay.net.data.Packet;
import ark.realmjay.net.packets.PacketType;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class UsePortalPacket implements Packet {
    public int objectId;

    public UsePortalPacket() { }

    @Override
    public PacketType getType() {
        return PacketType.USEPORTAL;
    }

    @Override
    public void write(DataOutput out) throws IOException {
        out.writeInt(this.objectId);
    }

    @Override
    public void read(DataInput in) throws IOException {
        this.objectId = in.readInt();
    }
}
