package ark.realmjay.net.packets.outgoing;

import ark.realmjay.net.data.Packet;
import ark.realmjay.net.packets.PacketType;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class SetConditionPacket implements Packet {
    public byte effect;

    public float duration;

    public PacketType getType() {
        return PacketType.SETCONDITION;
    }

    public void write(DataOutput out) throws IOException {
        out.writeByte(this.effect);
        out.writeFloat(this.duration);
    }

    public void read(DataInput in) throws IOException {
        this.effect = in.readByte();
        this.duration = in.readFloat();
    }
}
