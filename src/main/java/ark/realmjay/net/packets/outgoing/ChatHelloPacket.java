package ark.realmjay.net.packets.outgoing;

import ark.realmjay.net.data.Packet;
import ark.realmjay.net.packets.PacketType;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

@SuppressWarnings("WeakerAccess")
public class ChatHelloPacket implements Packet {
    public String accountId;
    public String token;

    public PacketType getType() {
        return PacketType.CHATHELLOMSG;
    }

    public void write(DataOutput out) throws IOException {
        out.writeUTF(this.accountId);
        out.writeUTF(this.token);
    }

    public void read(DataInput in) throws IOException {
        this.accountId = in.readUTF();
        this.token = in.readUTF();
    }
}
