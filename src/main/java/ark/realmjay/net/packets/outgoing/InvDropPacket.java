package ark.realmjay.net.packets.outgoing;

import ark.realmjay.net.data.Packet;
import ark.realmjay.net.data.SlotObjectData;
import ark.realmjay.net.packets.PacketType;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

@SuppressWarnings("ALL")
public class InvDropPacket implements Packet {
    public SlotObjectData slotObject;

    public PacketType getType() {
        return PacketType.INVDROP;
    }

    public InvDropPacket() { }

    public void write(DataOutput out) throws IOException {
        this.slotObject.write(out);
    }

    public void read(DataInput in) throws IOException {
        this.slotObject = new SlotObjectData();
        this.slotObject.read(in);
    }
}
