package ark.realmjay.net.packets.outgoing;

import ark.realmjay.net.data.Packet;
import ark.realmjay.net.packets.PacketType;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

@SuppressWarnings("WeakerAccess")
public class BuyPacket implements Packet {
    public int objectId;
    public int quantity;

    public PacketType getType() {
        return PacketType.BUY;
    }

    public BuyPacket() { }

    public void write(DataOutput out) throws IOException {
        out.writeInt(this.objectId);
        out.writeInt(this.quantity);
    }

    public void read(DataInput in) throws IOException {
        this.objectId = in.readInt();
        this.quantity = in.readInt();
    }
}
