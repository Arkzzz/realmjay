package ark.realmjay.net.packets.outgoing;

import ark.realmjay.net.data.Packet;
import ark.realmjay.net.packets.PacketType;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

@SuppressWarnings("WeakerAccess")
public class ChangeGuildRankPacket implements Packet {
    public String name;
    public int rank;

    @Override
    public PacketType getType() {
        return PacketType.CHANGEGUILDRANK;
    }

    @Override
    public void write(DataOutput out) throws IOException {
        out.writeUTF(this.name);
        out.writeInt(this.rank);
    }

    @Override
    public void read(DataInput in) throws IOException {
        this.name = in.readUTF();
        this.rank = in.readInt();
    }
}
