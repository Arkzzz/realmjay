package ark.realmjay.net.packets.outgoing;

import ark.realmjay.net.data.Packet;
import ark.realmjay.net.packets.PacketType;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class SquareHitPacket implements Packet {
    public int time;

    public byte bulletId;

    public int objectId;

    public SquareHitPacket() { }

    public PacketType getType() {
        return PacketType.SQUAREHIT;
    }

    public void write(DataOutput out) throws IOException {
        out.writeInt(this.time);
        out.writeByte(this.bulletId);
        out.writeInt(this.objectId);
    }

    public void read(DataInput in) throws IOException {
        this.time = in.readInt();
        this.bulletId = in.readByte();
        this.objectId = in.readInt();
    }
}
