package ark.realmjay.net.packets.outgoing;

import ark.realmjay.net.data.Packet;
import ark.realmjay.net.data.SlotObjectData;
import ark.realmjay.net.data.WorldPosData;
import ark.realmjay.net.packets.PacketType;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class UseItemPacket implements Packet {
    public int time;

    public SlotObjectData slotObject;

    public WorldPosData pos;

    public byte useType;

    public UseItemPacket() { }

    @Override
    public PacketType getType() {
        return PacketType.USEITEM;
    }

    @Override
    public void write(DataOutput out) throws IOException {
        out.writeInt(this.time);
        this.slotObject.write(out);
        this.pos.write(out);
        out.writeByte(this.useType);
    }

    @Override
    public void read(DataInput in) throws IOException {
        this.time = in.readInt();
        this.slotObject = new SlotObjectData();
        this.slotObject.read(in);
        this.pos = new WorldPosData();
        this.pos.read(in);
        this.useType = in.readByte();
    }
}
