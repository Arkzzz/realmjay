package ark.realmjay.net.packets.outgoing;

import ark.realmjay.net.data.Packet;
import ark.realmjay.net.packets.PacketType;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class LoadPacket implements Packet {
    public int charId;
    public boolean isFromArena;

    public PacketType getType() {
        return PacketType.LOAD;
    }

    public LoadPacket() { }

    public void write(DataOutput out) throws IOException {
        out.writeInt(this.charId);
        out.writeBoolean(this.isFromArena);
    }

    public void read(DataInput in) throws IOException {
        this.charId = in.readInt();
        this.isFromArena = in.readBoolean();
    }
}
