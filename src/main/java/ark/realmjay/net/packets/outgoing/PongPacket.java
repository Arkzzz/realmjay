package ark.realmjay.net.packets.outgoing;

import ark.realmjay.net.data.Packet;
import ark.realmjay.net.packets.PacketType;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class PongPacket implements Packet {
    public int serial;

    public int time;

    public PongPacket() { }

    public PacketType getType() {
        return PacketType.PONG;
    }

    public void write(DataOutput out) throws IOException {
        out.writeInt(this.serial);
        out.writeInt(this.time);
    }

    public void read(DataInput in) throws IOException {
        this.serial = in.readInt();
        this.time = in.readInt();
    }
}
