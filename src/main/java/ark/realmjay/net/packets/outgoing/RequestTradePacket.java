package ark.realmjay.net.packets.outgoing;

import ark.realmjay.net.data.Packet;
import ark.realmjay.net.packets.PacketType;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class RequestTradePacket implements Packet {
    public String name;

    public RequestTradePacket() { }

    public PacketType getType() {
        return PacketType.REQUESTTRADE;
    }

    public void write(DataOutput out) throws IOException {
        out.writeUTF(this.name);
    }

    public void read(DataInput in) throws IOException {
        this.name = in.readUTF();
    }
}
