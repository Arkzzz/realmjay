package ark.realmjay.net.packets.outgoing;

import ark.realmjay.net.data.Packet;
import ark.realmjay.net.data.WorldPosData;
import ark.realmjay.net.packets.PacketType;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

@SuppressWarnings("ALL")
public class GroundDammagePacket implements Packet {
    public int time;

    public WorldPosData pos;

    public PacketType getType() {
        return PacketType.GROUNDDAMAGE;
    }

    public GroundDammagePacket() {
        this.pos = new WorldPosData();
    }

    public void write(DataOutput out) throws IOException {
        out.writeInt(this.time);
        this.pos.write(out);
    }

    public void read(DataInput in) throws IOException {
        this.time = in.readInt();
        this.pos = new WorldPosData();
        this.pos.read(in);
    }
}
