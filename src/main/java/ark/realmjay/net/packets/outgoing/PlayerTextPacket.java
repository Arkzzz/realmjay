package ark.realmjay.net.packets.outgoing;

import ark.realmjay.net.data.Packet;
import ark.realmjay.net.packets.PacketType;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

@SuppressWarnings("ALL")
public class PlayerTextPacket implements Packet {
    public String text;

    public PlayerTextPacket() { }

    public PacketType getType() {
        return PacketType.PLAYERTEXT;
    }

    public void write(DataOutput out) throws IOException {
        out.writeUTF(this.text);
    }

    public void read(DataInput in) throws IOException {
        this.text = in.readUTF();
    }
}
