package ark.realmjay.game;

@SuppressWarnings("WeakerAccess")
public enum ConditionEffect {
    NOTHING("Nothing", 0),
    DEAD("Dead", 1),
    QUIET("Quiet", 2),
    WEAK("Weak", 3),
    SLOWED("Slowed", 4),
    SICK("Sick", 5),
    DAZED("Dazed", 6),
    STUNNED("Stunned", 7),
    BLIND("Blind", 8),
    HALLUCINATING("Hallucinating", 9),
    DRUNK("Drunk", 10),
    CONFUSED("Confused", 11),
    STUN_IMMUNE("Stun Immune", 12),
    INVISIBLE("Invisible", 13),
    PARALYZED("Paralyzed", 14),
    SPEEDY("Speedy", 15),
    BLEEDING("Bleeding", 16),
    NOT_USED("Not Used", 17),
    HEALING("Healing", 18),
    DAMAGING("Damaging", 19),
    BERSERK("Berserk", 20),
    PAUSED("Paused", 21),
    STASIS("Stasis", 22),
    STASIS_IMMUNE("Stasis Immune", 23),
    INVINCIBLE("Invincible", 24),
    INVULNERABLE("Invulnerable", 25),
    ARMORED("Armored", 26),
    ARMORBROKEN("Armor Broken", 27),
    HEXED("Hexed", 28),
    NINJA_SPEEDY("Ninja Speedy", 29);

    String name;
    int id;
    float duration;

    ConditionEffect(String name, int id) {
        this.name = name;
        this.id = id;
        this.duration = 0;
    }

    public static ConditionEffect fromObject(String name, float duration) {
        ConditionEffect effect = ConditionEffect.fromName(name);
        effect.duration = duration;
        return effect;
    }

    public static ConditionEffect fromName(String name) {
        for (ConditionEffect c : ConditionEffect.values()) {
            if (c.name.equals(name)) return c;
        }
        return ConditionEffect.NOTHING;
    }

    public static boolean has(int condition, ConditionEffect effect) {
        int effbit = 1 << effect.id - 1;
        return (condition & effbit) == 1;
    }
}
