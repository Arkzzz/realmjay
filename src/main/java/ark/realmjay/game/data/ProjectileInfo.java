package ark.realmjay.game.data;

import ark.realmjay.game.ConditionEffect;
import ark.realmjay.util.OptionalXml;
import org.jdom2.Element;

import java.util.ArrayList;

public class ProjectileInfo {
    public ProjectileInfo(Element e) {
        this.id = Byte.parseByte(OptionalXml.attribute(e, "id", "0"));

        this.damage = OptionalXml.childInt(e,"Damage", 0);
        this.speed = OptionalXml.childFloat(e, "Speed", 0);
        this.size = OptionalXml.childInt(e, "Size", 0);
        this.lifetime = OptionalXml.childFloat(e, "LifetimeMS", 0);

        this.minDamage = OptionalXml.childInt(e, "MinDamage", 0);
        this.maxDamage = OptionalXml.childInt(e, "MaxDamage", 0);

        this.magnitude = OptionalXml.childFloat(e, "Magnitude", 0);
        this.amplitude = OptionalXml.childFloat(e, "Amplitude", 0);

        this.wavy = OptionalXml.child(e, "Wavy");
        this.parametric = OptionalXml.child(e, "Parametric");
        this.boomerang = OptionalXml.child(e,"Boomerang");
        this.armorPierce = OptionalXml.child(e, "ArmorPiercing");
        this.multiHit = OptionalXml.child(e, "MultiHit");
        this.passesCover = OptionalXml.child(e, "PassesCover");

        this.effects = new ArrayList<ConditionEffect>();
        for (Element elm : e.getChildren("ConditionEffect")) {
            this.effects.add(ConditionEffect.fromObject(
                    elm.getValue(),
                    Float.parseFloat(elm.getAttributeValue("duration"))
            ));
        }

        this.texture = OptionalXml.child(e, "Texture") ? new TextureInfo(e) : TextureInfo.DEFAULT;
    }

    private byte id;

    private int damage;
    private float speed;
    private int size;
    private float lifetime;

    private int minDamage;
    private int maxDamage;

    private float magnitude;
    private float amplitude;
    private float frequency;

    private boolean wavy;
    private boolean parametric;
    private boolean boomerang;
    private boolean armorPierce;
    private boolean multiHit;
    private boolean passesCover;

    private ArrayList<ConditionEffect> effects;

    private TextureInfo texture;

    public byte getId() {
        return id;
    }

    public int getDamage() {
        return damage;
    }

    public float getSpeed() {
        return speed;
    }

    public int getSize() {
        return size;
    }

    public float getLifetime() {
        return lifetime;
    }

    public int getMinDamage() {
        return minDamage;
    }

    public int getMaxDamage() {
        return maxDamage;
    }

    public float getMagnitude() {
        return magnitude;
    }

    public float getAmplitude() {
        return amplitude;
    }

    public float getFrequency() {
        return frequency;
    }

    public boolean isWavy() {
        return wavy;
    }

    public boolean isParametric() {
        return parametric;
    }

    public boolean isBoomerang() {
        return boomerang;
    }

    public boolean isArmorPierce() {
        return armorPierce;
    }

    public boolean isMultiHit() {
        return multiHit;
    }

    public boolean isPassesCover() {
        return passesCover;
    }

    public ArrayList<ConditionEffect> getEffects() {
        return effects;
    }

    public TextureInfo getTexture() {
        return texture;
    }
}
