package ark.realmjay.game.data;

import ark.realmjay.util.OptionalXml;
import org.jdom2.Element;

import java.util.ArrayList;

@SuppressWarnings("WeakerAccess")
public class ObjectInfo {
    public static final ObjectInfo DEFAULT = new ObjectInfo();
    /**
     * Name of the object.
     */
    private String name;
    /**
     * Id of the object.
     */
    private int id;

    /**
     * What type of object.
     */
    private String objectClass;

    /**
     * Maximum hitpoints of the object.
     */
    private int maxHp;

    /**
     * The amount of exp gained when the object dies.
     */
    private float xpMultiplier;

    /**
     * Weather or not the object can be walked over.
     */
    private boolean isStatic;
    private boolean occupySquare;
    private boolean enemyOccupySquare;
    private boolean fullOccupy;
    private boolean blocksSight;

    /**
     * Weather or not the object protects the player from ground damage.
     */
    private boolean protectsFromGroundDammage;

    /**
     * Weather or not the object protects the player from sinking.
     */
    private boolean protectsFromSink;

    /**
     * Weather or not the object is an enemy.
     */
    private boolean enemy;

    /**
     * Weather or not the object is a player.
     */
    private boolean player;

    /**
     * Weather or not the object is a pet.
     */
    private boolean pet;

    private int size;

    private int shadowSize;

    private boolean drawOnGround;
    /**
     * How much defense the object child.
     */
    private int defence;

    /**
     * Weather the object is flying.
     */
    private boolean flying;

    /**
     * Weather or not the object counts towards god kills.
     */
    private boolean god;

    /**
     * Weather or not the object is a quest target.
     */
    private boolean quest;

    /**
     * Weather or not the object is an item.
     */
    private boolean item;

    /**
     * Weather or not the item can be traded.
     */
    private boolean soulbound;

    /**
     * Weather or not the object is usable.
     */
    private boolean usable;

    public ArrayList<ProjectileInfo> getProjectiles() {
        return projectiles;
    }

    /**
     * MP cost of using to object.
     */
    private int mpCost;

    /**
     * Projectiles
     */
    private ArrayList<ProjectileInfo> projectiles;

    private TextureInfo texture;
    private TextureInfo animatedTexture;
    public ObjectInfo(Element e) {
        this.id = OptionalXml.attributeIntDecode(e, "type", -1);
        this.name = OptionalXml.attribute(e, "id", "unknown");
        this.objectClass = OptionalXml.child(e, "Class", "unknown");

        this.maxHp = OptionalXml.childIntHex(e, "MaxHitPoints", 0);
        this.xpMultiplier = OptionalXml.childFloat(e,"XpMult", 0);

        this.isStatic = OptionalXml.child(e, "Static");
        this.occupySquare = OptionalXml.child(e, "OccupySquare");
        this.enemyOccupySquare = OptionalXml.child(e, "EnemyOccupySquare");
        this.fullOccupy = OptionalXml.child(e, "FullOccupy");
        this.blocksSight = OptionalXml.child(e, "BlocksSight");
        this.protectsFromGroundDammage = OptionalXml.child(e, "ProtectFromGroundDamage");
        this.protectsFromSink = OptionalXml.child(e, "ProtectFromSink");
        this.enemy = OptionalXml.child(e, "Enemy");
        this.player = OptionalXml.child(e, "Player");
        this.pet = OptionalXml.child(e, "Pet");
        this.drawOnGround = OptionalXml.child(e, "DrawOnGround");

        this.size = OptionalXml.childInt(e, "Size", 0);
        this.shadowSize = OptionalXml.childInt(e, "ShadowSize", 0);
        this.defence = OptionalXml.childInt(e, "Defence", 0);
        this.flying = OptionalXml.child(e, "Flying");
        this.god = OptionalXml.child(e, "God");
        this.quest = OptionalXml.child(e, "Quest");

        this.item = OptionalXml.child(e, "Item");
        this.usable = OptionalXml.child(e, "Usable");
        this.soulbound = OptionalXml.child(e, "Soulbound");

        this.mpCost = OptionalXml.childInt(e,"MpCost", 0);
        this.projectiles = new ArrayList<>();
        if (OptionalXml.child(e, "Projectile")) {
            for (Element elm : e.getChildren("Projectile")) {
                this.projectiles.add(new ProjectileInfo(elm));
            }
        }

        this.texture = OptionalXml.child(e, "Texture") ? new TextureInfo(e.getChild("Texture")) : TextureInfo.DEFAULT;
        this.animatedTexture = OptionalXml.child(e, "AnimatedTexture") ? new TextureInfo(e.getChild("AnimatedTexture")) : TextureInfo.DEFAULT;
    }

    private ObjectInfo() {
        this.id = -1;
        this.name = "unknown";
        this.objectClass = "unknown";
        this.maxHp = 0;
        this.xpMultiplier = 0;
        this.isStatic = false;
        this.occupySquare = false;
        this.enemyOccupySquare = false;
        this.fullOccupy = false;
        this.blocksSight = false;
        this.protectsFromGroundDammage = false;
        this.protectsFromSink = false;
        this.enemy = false;
        this.player = false;
        this.pet = false;
        this.drawOnGround = false;
        this.size = 0;
        this.defence = 0;
        this.flying = false;
        this.god = false;
        this.quest = false;
        this.item = false;
        this.usable = false;
        this.soulbound = false;
        this.mpCost = 0;
        this.projectiles = new ArrayList<>();
        this.texture = TextureInfo.DEFAULT;
        this.animatedTexture = TextureInfo.DEFAULT;
    }


    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public String getObjectClass() {
        return objectClass;
    }

    public int getMaxHp() {
        return maxHp;
    }

    public float getXpMultiplier() {
        return xpMultiplier;
    }

    public boolean isStatic() {
        return isStatic;
    }

    public boolean isOccupySquare() {
        return occupySquare;
    }

    public boolean isEnemyOccupySquare() {
        return enemyOccupySquare;
    }

    public boolean isFullOccupy() {
        return fullOccupy;
    }

    public boolean isBlocksSight() {
        return blocksSight;
    }

    public boolean isProtectsFromGroundDammage() {
        return protectsFromGroundDammage;
    }

    public boolean isProtectsFromSink() {
        return protectsFromSink;
    }

    public boolean isEnemy() {
        return enemy;
    }

    public boolean isPlayer() {
        return player;
    }

    public boolean isPet() {
        return pet;
    }

    public int getSize() {
        return size;
    }

    public int getShadowSize() {
        return shadowSize;
    }

    public boolean isDrawOnGround() {
        return drawOnGround;
    }

    public int getDefence() {
        return defence;
    }

    public boolean isFlying() {
        return flying;
    }

    public boolean isGod() {
        return god;
    }

    public boolean isQuest() {
        return quest;
    }

    public boolean isItem() {
        return item;
    }

    public boolean isSoulbound() {
        return soulbound;
    }

    public boolean isUsable() {
        return usable;
    }

    public int getMpCost() {
        return mpCost;
    }

    public TextureInfo getTexture() {
        return texture;
    }

    public TextureInfo getAnimatedTexture() {
        return animatedTexture;
    }
}
