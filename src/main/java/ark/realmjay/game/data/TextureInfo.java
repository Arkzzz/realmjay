package ark.realmjay.game.data;

import ark.realmjay.util.OptionalXml;
import org.jdom2.Element;

public class TextureInfo {
    public static final TextureInfo DEFAULT = new TextureInfo();
    private String file;
    private int index;

    public TextureInfo(Element e) {
        this.file = OptionalXml.child(e, "File", "unknown");
        this.index = OptionalXml.childIntDecode(e,"Index", 0);
    }

    private TextureInfo() {
        this.file = "unknown";
        this.index = 0;
    }

    public String getFile() {
        return file;
    }

    public int getIndex() {
        return index;
    }
}
