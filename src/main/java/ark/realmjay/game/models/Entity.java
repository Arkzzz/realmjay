package ark.realmjay.game.models;

import ark.realmjay.game.data.ObjectInfo;
import ark.realmjay.net.data.ObjectStatusData;

@SuppressWarnings("ALL")
public class Entity {
    protected int objectId;
    protected Vector2 pos;
    protected ObjectInfo info;

    public Entity(ObjectStatusData data, ObjectInfo info) {
        this.objectId = data.objectId;
        this.pos = data.pos.clone();
        this.info = info;
    }

    public Entity(int objectId, Vector2 pos, ObjectInfo info) {
        this.objectId = objectId;
        this.pos = pos;
        this.info = info;
    }

    public int getObjectId() {
        return objectId;
    }

    public Vector2 getPos() {
        return pos;
    }

    public ObjectInfo getInfo() {
        return info;
    }

    public void setPos(float x, float y) {
        pos.x = x;
        pos.y = y;
    }
}
