package ark.realmjay.game.models;

public class Account {
    public String name;
    public String guid;
    public String password;
    public Account(String guid, String password) {
        this.guid = guid;
        this.password = password;
        this.name = guid;
    }
}
